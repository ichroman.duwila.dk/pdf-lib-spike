import com.lowagie.text.pdf.BaseFont
import java.io.File
import java.io.FileOutputStream
import java.io.StringWriter
import java.nio.file.Path
import java.nio.file.Paths
import java.util.Properties
import org.apache.fontbox.ttf.OTFParser
import org.apache.velocity.VelocityContext
import org.apache.velocity.app.VelocityEngine
import org.jsoup.Jsoup
import org.xhtmlrenderer.layout.SharedContext
import org.xhtmlrenderer.pdf.ITextRenderer
import org.xhtmlrenderer.swing.NaiveUserAgent


fun main(args: Array<String>) {
    printElapsedTimeInSeconds({
        val engine = VelocityEngine()
        engine.init(
            Properties().apply {
                this["file.resource.loader.path"] = ClassLoader.getSystemClassLoader().getResource("flyingsaucer/").path
            }
        )

        val template = engine.getTemplate("sharia_advice.vm")
        val context = VelocityContext()
        context.put("customerName", "Abdullah bin Abu Bakar bin Jufri")
        context.put("accountNumber", "700101981291")
        context.put("adviceDocumentNumber", "70010198129120240430")
        context.put("creationDate", "29 April 2024")
        context.put("tenure", "1 Bulan")
        context.put("maturityDate", "29 May 2024")
        context.put("nisbahRate", "16,21%")
        context.put("rollOverMode", "Tidak diperpanjang otomatis")
        context.put("contract", "Mudharabah Muthlaqah")
        context.put("principalAmount", "Rp1.234.567")
        context.put("principalAmountInWords", "Satu juta dua ratus tiga puluh empat ribu lima ratus enam puluh tujuh rupiah")

        val writer = StringWriter()
        template.merge(context, writer)
        println(writer.toString())

        val htmlFile = File(ClassLoader.getSystemResource("flyingsaucer/template.html").toURI())
        val document = Jsoup.parse(writer.toString(), "UTF-8")
        val path = "${Paths.get("").toAbsolutePath().toString()}/results/flyingsaucer"
        document.outputSettings().syntax(org.jsoup.nodes.Document.OutputSettings.Syntax.xml)

        val outputStream = FileOutputStream("$path/flyingsaucer.pdf")
        val renderer = ITextRenderer()
        val sharedContext: SharedContext = renderer.sharedContext
        sharedContext.isPrint = true
        sharedContext.isInteractive = false

        val baseUrl = Paths.get(ClassLoader.getSystemClassLoader().getResource("flyingsaucer/").toURI())
        println("## baseUrl = $baseUrl")

        renderer.fontResolver.addFont(
            "$baseUrl/TT Commons DemiBold.otf",
            "TTCommonsDemiBold",
            BaseFont.CP1252,
            true,
            null
        )
        renderer.fontResolver.addFont(
            "$baseUrl/TT Commons Regular.otf",
            "TTCommonsRegular",
            BaseFont.CP1252,
            true,
            null
        )
        renderer.setDocumentFromString(document.html(), "file://$baseUrl")

        renderer.layout()
        renderer.createPDF(outputStream)
    }, "Convert html to pdf")
}