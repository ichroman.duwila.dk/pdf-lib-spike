import org.apache.pdfbox.pdmodel.graphics.color.PDColor
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceRGB
import java.io.File
import java.time.Duration
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import org.apache.fontbox.ttf.OTFParser

object Constants {
    const val DOCUMENT_AUTHOR = "PT. Bank Jago, Tbk"
    const val DOCUMENT_TITLE = "PDF Document"
    const val DOCUMENT_CREATOR = "Money Management"
    const val DOCUMENT_SUBJECT = "Advis Deposito TD Mudharabah"

    const val headerImagePath = "./assets/images/sharia_advice_header.png"
    const val lpsLogoPath = "./assets/images/logo-lps-id.png"

    val BLACK_TEXT_COLOR = PDColor(floatArrayOf(0.0f, 0.0f, 0.0f), PDDeviceRGB.INSTANCE)
    val GREY_TEXT_COLOR = PDColor(floatArrayOf(0.5f, 0.5f, 0.5f), PDDeviceRGB.INSTANCE)
    val LIGHT_GREY_TEXT_COLOR = PDColor(floatArrayOf(0.8f, 0.8f, 0.8f), PDDeviceRGB.INSTANCE)
    val PURPLE_STROKE_COLOR = PDColor(floatArrayOf(0.5f, 0.0f, 0.5f), PDDeviceRGB.INSTANCE)
    val WHITE_FILL_COLOR = PDColor(floatArrayOf(1.0f, 1.0f, 1.0f), PDDeviceRGB.INSTANCE)

    val TTCOMMON_REGULAR_FONT = File(ClassLoader.getSystemClassLoader().getResource("assets/fonts/TTCommonsRegular.ttf")?.path!!)
    val TTCOMMON_DEMIBOLD_FONT = File(ClassLoader.getSystemClassLoader().getResource("assets/fonts/TTCommonsDemiBold.ttf")?.path!!)

    const val ALIGN_LEFT = "left"
    const val ALIGN_RIGHT = "right"
    const val ALIGN_CENTER = "center"

    const val fontSize8 = 8
    const val fontSize9 = 9
    const val fontSize10 = 10
    const val fontSize12 = 12
    const val fontSize14 = 14
    const val fontSize15 = 15
    const val fontSize16 = 16

    const val contentWidth = 575f
    const val leftBoundPoint = 20f
    const val fieldBoxHeight = 27f
    const val amountFieldBoxHeight = 32f

    const val commonPropertyValueXIncrementPos = 9f
    const val commonPropertyValueYIncrementPos = 16f
}


inline fun printElapsedTimeInSeconds(callback: () -> Unit, processName: String) {
    val start = Instant.now()

    // execute the callback
    callback()

    val end  = Instant.now()

    val elapsedTime = Duration.between(start, end)
    val elapsedTimeInSeconds = elapsedTime.toMillis().toDouble() / 1000

    println("## Elapsed time ($processName) = ${elapsedTimeInSeconds}s")
}