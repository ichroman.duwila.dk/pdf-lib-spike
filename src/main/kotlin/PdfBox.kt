import Constants.ALIGN_CENTER
import Constants.ALIGN_LEFT
import Constants.ALIGN_RIGHT
import Constants.BLACK_TEXT_COLOR
import Constants.DOCUMENT_AUTHOR
import Constants.DOCUMENT_CREATOR
import Constants.DOCUMENT_SUBJECT
import Constants.DOCUMENT_TITLE
import Constants.GREY_TEXT_COLOR
import Constants.LIGHT_GREY_TEXT_COLOR
import Constants.PURPLE_STROKE_COLOR
import Constants.TTCOMMON_DEMIBOLD_FONT
import Constants.TTCOMMON_REGULAR_FONT
import Constants.WHITE_FILL_COLOR
import Constants.amountFieldBoxHeight
import Constants.commonPropertyValueXIncrementPos
import Constants.commonPropertyValueYIncrementPos
import Constants.contentWidth
import Constants.fieldBoxHeight
import Constants.fontSize10
import Constants.fontSize12
import Constants.fontSize14
import Constants.fontSize15
import Constants.fontSize16
import Constants.fontSize8
import Constants.fontSize9
import Constants.headerImagePath
import Constants.leftBoundPoint
import Constants.lpsLogoPath
import java.io.ByteArrayOutputStream
import java.io.File
import java.nio.file.Paths
import java.time.ZonedDateTime
import java.util.GregorianCalendar
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.pdmodel.PDPage
import org.apache.pdfbox.pdmodel.PDPageContentStream
import org.apache.pdfbox.pdmodel.common.PDRectangle
import org.apache.pdfbox.pdmodel.font.PDType0Font
import org.apache.pdfbox.pdmodel.graphics.color.PDColor
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject

class ImageCache(private val pdDocument: PDDocument) {
    private val cache = mutableMapOf<String, PDImageXObject>()

    fun getImage(path: String): PDImageXObject {
        return cache.getOrPut(path) {
            PDImageXObject.createFromFile(
                ClassLoader.getSystemClassLoader().getResource(path)?.path!!, pdDocument
            )
        }
    }
}

data class Payload(
    val customerName: String,
    val accountNumber: String,
    val adviceDocumentNumber: String,
    val createdDate: String,
    val tenure: String,
    val maturityDate: String,
    val nisbahRate: String,
    val principalAmount: String,
    val principalAmountInWords: String,
    val contract: String,
    val rollOverMode: String
)

class PdfDocumentCreator(private val payload: Payload) {
    private val pdDocument = PDDocument()
    private val imageCache = ImageCache(pdDocument)
    private val regularFont = PDType0Font.load(pdDocument, TTCOMMON_REGULAR_FONT)
    private val demiBoldFont = PDType0Font.load(pdDocument, TTCOMMON_DEMIBOLD_FONT)

    private lateinit var contentStream: PDPageContentStream
    private lateinit var page: PDPage

    fun createDocument(): ByteArrayOutputStream {
        // set metadata and init page and stream for filling contents
        setMetadata()
        page = addPage()
        contentStream = createContentStream(page)

        setHeaderImage()
        addText(payload.customerName, leftBoundPoint, 97f, demiBoldFont, fontSize16, BLACK_TEXT_COLOR)
        addText("Nomor Kantong Deposito ${payload.accountNumber}", leftBoundPoint, 110f, regularFont, fontSize12, GREY_TEXT_COLOR)

        addText("No. Advis", leftBoundPoint, 97f, regularFont, fontSize10, GREY_TEXT_COLOR, ALIGN_RIGHT)
        addText(payload.adviceDocumentNumber, leftBoundPoint, 110f, demiBoldFont, fontSize12, GREY_TEXT_COLOR, ALIGN_RIGHT)

        addLine(contentWidth, leftBoundPoint, 134f)

        addText("Advis Deposito", leftBoundPoint, 168f, demiBoldFont, fontSize15, BLACK_TEXT_COLOR, ALIGN_CENTER)

        setFirstRowForm()
        setSecondRowForm()
        setThirdRowForm()
        setFourthRowForm()

        addNote()
        addLine(contentWidth, leftBoundPoint, 405f)

        addDisclaimer()

        addFooter()

        contentStream.close()
        return saveDocument()
    }

    private fun setFirstRowForm() {
        // Define the content width, padding, and rectangle height
        val yAxis = 198f
        val valueYAxis = yAxis + commonPropertyValueYIncrementPos
        val contentWidth = 575f - leftBoundPoint
        val padding = 5f

        // Calculate the width of each rectangle
        val rectangleWidth = (contentWidth - padding * 3) / 4

        // Draw the rectangles
        val box1 = leftBoundPoint + 0 * (rectangleWidth + padding)
        val box2 = leftBoundPoint + 1 * (rectangleWidth + padding)
        val box3 = leftBoundPoint + 2 * (rectangleWidth + padding)
        val box4 = leftBoundPoint + 3 * (rectangleWidth + padding)

        drawRectangle(
            box1,
            yAxis,
            rectangleWidth,
            fieldBoxHeight,
            PURPLE_STROKE_COLOR,
            WHITE_FILL_COLOR,
            "Tanggal Pembuatan",
        )
        addText(
            payload.createdDate,
            box1 + commonPropertyValueXIncrementPos,
            valueYAxis,
            demiBoldFont,
            fontSize9,
            BLACK_TEXT_COLOR,
        )

        drawRectangle(
            box2,
            yAxis,
            rectangleWidth,
            fieldBoxHeight,
            PURPLE_STROKE_COLOR,
            WHITE_FILL_COLOR,
            "Durasi",
        )
        addText(
            payload.tenure,
            box2 + commonPropertyValueXIncrementPos,
            valueYAxis,
            demiBoldFont,
            fontSize9,
            BLACK_TEXT_COLOR,
        )

        drawRectangle(
            box3,
            yAxis,
            rectangleWidth,
            fieldBoxHeight,
            PURPLE_STROKE_COLOR,
            WHITE_FILL_COLOR,
            "Tanggal Jatuh Tempo",
        )
        addText(
            payload.maturityDate,
            box3 + commonPropertyValueXIncrementPos,
            valueYAxis,
            demiBoldFont,
            fontSize9,
            BLACK_TEXT_COLOR,
        )

        drawRectangle(
            box4,
            yAxis,
            rectangleWidth,
            fieldBoxHeight,
            PURPLE_STROKE_COLOR,
            WHITE_FILL_COLOR,
            "Nisbah Untuk Nasabah",
        )
        addText(
            payload.nisbahRate,
            box4 + commonPropertyValueXIncrementPos,
            valueYAxis,
            demiBoldFont,
            fontSize9,
            BLACK_TEXT_COLOR,
        )
    }

    private fun setSecondRowForm() {
        // Define the content width, padding, and rectangle height
        val yAxis = 242f
        val valueYAxis = yAxis + commonPropertyValueYIncrementPos
        val contentWidth = 575f - leftBoundPoint
        val padding = 5f

        // Calculate the width of each rectangle
        val rectangleWidth = (contentWidth - padding) / 2

        // Draw the rectangles
        val box1 = leftBoundPoint + 0 * (rectangleWidth + padding)
        val box2 = leftBoundPoint + 1 * (rectangleWidth + padding)

        drawRectangle(
            box1,
            yAxis,
            rectangleWidth,
            fieldBoxHeight,
            PURPLE_STROKE_COLOR,
            WHITE_FILL_COLOR,
            "Mode Perpanjang Deposito",
        )
        addText(
            payload.rollOverMode,
            box1 + commonPropertyValueXIncrementPos,
            valueYAxis,
            demiBoldFont,
            fontSize9,
            BLACK_TEXT_COLOR
        )

        drawRectangle(
            box2,
            yAxis,
            rectangleWidth,
            fieldBoxHeight,
            PURPLE_STROKE_COLOR,
            WHITE_FILL_COLOR,
            "Akad",
        )
        addText(
            payload.contract,
            box2 + commonPropertyValueXIncrementPos,
            valueYAxis,
            demiBoldFont,
            fontSize9,
            BLACK_TEXT_COLOR
        )
    }

    private fun setThirdRowForm() {
        // Define the content width, padding, and rectangle height
        val yAxis = 286f
        val contentWidth = 575f - leftBoundPoint

        // Draw the rectangles
        val box = leftBoundPoint

        drawRectangle(
            box,
            yAxis,
            contentWidth,
            amountFieldBoxHeight,
            PURPLE_STROKE_COLOR,
            WHITE_FILL_COLOR,
            "Nominal Pokok",
        )
        addText(
            payload.principalAmount,
            leftBoundPoint + commonPropertyValueXIncrementPos,
            yAxis + commonPropertyValueXIncrementPos + 12f,
            demiBoldFont,
            fontSize15,
            BLACK_TEXT_COLOR,
        )
    }

    private fun setFourthRowForm() {
        // Define the content width, padding, and rectangle height
        val yAxis = 327f
        val contentWidth = 575f - leftBoundPoint

        drawRectangle(
            leftBoundPoint,
            yAxis,
            contentWidth,
            fieldBoxHeight,
            PURPLE_STROKE_COLOR,
            WHITE_FILL_COLOR,
            "Terbilang",
        )
        addText(
            payload.principalAmountInWords,
            leftBoundPoint + commonPropertyValueXIncrementPos,
            yAxis + commonPropertyValueYIncrementPos,
            demiBoldFont,
            fontSize9,
            BLACK_TEXT_COLOR
        )
    }

    private fun addNote() {
        addText(
            "Advis ini adalah bukti kepemilikan yang sah atas Deposito Syariah yang diterbitkan oleh PT Bank Jago Tbk. - Unit Usaha Syariah",
            65f, 380f, regularFont, fontSize10, LIGHT_GREY_TEXT_COLOR, ALIGN_CENTER
        )
    }

    private fun addDisclaimer() {
        val fontSize = 8
        addText("Disclaimer", leftBoundPoint, 433f, demiBoldFont, fontSize10, BLACK_TEXT_COLOR)
        addText(
            "1.   Advis ini merupakan bukti penempatan Deposito Syariah di PT Bank Jago Tbk. - Unit Usaha Syariah",
            leftBoundPoint, 455f, regularFont, fontSize, BLACK_TEXT_COLOR
        )
        addText(
            "2.  Advis ini tidak dapat dijaminkan atau digadaikan kepada siapapun",
            leftBoundPoint, 468f, regularFont, fontSize, BLACK_TEXT_COLOR
        )
        addText(
            "3.  Advis ini diterbitkan oleh sistem dan sah tanpa memerlukan tanda tangan pejabat Bank",
            leftBoundPoint, 482f, regularFont, fontSize, BLACK_TEXT_COLOR
        )
        addText(
            "4.  Syarat dan Ketentuan Deposito Syariah merupakan satu kesatuan yang tidak terpisahkan dari Syarat dan Ketentuan Nasabah Bank Jago Unit Usaha Syariah",
            leftBoundPoint, 495f, regularFont, fontSize, BLACK_TEXT_COLOR
        )
    }

    private fun addFooter() {
        val logoWidth = 63f
        val logoHeight = 40f
        val yAxis = page.mediaBox.height - 20f - logoHeight
        // add image
        val lpsLogo = imageCache.getImage(lpsLogoPath)
        addImage(
            lpsLogo,
            leftBoundPoint,
            yAxis,
            logoWidth,
            logoHeight
        )

        val descXAxis = 40f + logoWidth
        val descYAxis = yAxis + 15f
        addText(
            "PT Bank Jago Tbk berizin dan diawasi oleh Otoritas Jasa Keuangan (OJK)",
            descXAxis,
            descYAxis,
            regularFont,
            fontSize9,
            LIGHT_GREY_TEXT_COLOR
        )
        addText(
            "serta merupakan peserta penjaminan Lembaga Penjamin Simpanan (LPS).",
            descXAxis,
            descYAxis + 10f,
            regularFont,
            fontSize9,
            LIGHT_GREY_TEXT_COLOR
        )

        val urlXAxis = page.mediaBox.width - (page.mediaBox.width - leftBoundPoint)
        addText(
            "www.jago.com",
            urlXAxis,
            descYAxis + 4f,
            demiBoldFont,
            fontSize9,
            BLACK_TEXT_COLOR,
            "right",
        )
    }

    private fun setMetadata() {
        pdDocument.documentInformation.also { docInfo ->
            docInfo.author = DOCUMENT_AUTHOR
            docInfo.title = DOCUMENT_TITLE
            docInfo.creator = DOCUMENT_CREATOR
            docInfo.subject = DOCUMENT_SUBJECT
            docInfo.creationDate = GregorianCalendar.from(ZonedDateTime.now())
        }
    }

    private fun addPage(): PDPage {
        val page = PDPage(PDRectangle.A4)
        pdDocument.addPage(page)
        return page
    }

    private fun createContentStream(page: PDPage): PDPageContentStream {
        return PDPageContentStream(pdDocument, page)
    }

    private fun setHeaderImage() {
        val headerImage = imageCache.getImage(headerImagePath)
        addImage(headerImage, 0f, 0f, 595f, 66f)
    }

    private fun addImage(
        image: PDImageXObject,
        x: Float,
        y: Float,
        width: Float,
        height: Float
    ) {
        val adjustedY = page.mediaBox.height - y - height
        contentStream.drawImage(image, x, adjustedY, width, height)
    }

    private fun addText(
        text: String,
        x: Float,
        y: Float,
        font: PDType0Font,
        fontSize: Int,
        color: PDColor,
        alignment: String = ALIGN_LEFT
    ) {
        contentStream.setFont(font, fontSize.toFloat())
        contentStream.setNonStrokingColor(color)

        val textWidth = font.getStringWidth(text) / 1000 * fontSize
        val adjustedX = when (alignment) {
            ALIGN_RIGHT -> page.mediaBox.width - x - textWidth
            ALIGN_CENTER -> (page.mediaBox.width - textWidth) / 2
            else -> x
        }

        contentStream.beginText()
        contentStream.newLineAtOffset(adjustedX, page.mediaBox.height - y)
        contentStream.showText(text)
        contentStream.endText()
    }

    private fun addLine(width: Float, x: Float, y: Float) {
        contentStream.setStrokingColor(LIGHT_GREY_TEXT_COLOR)
        contentStream.moveTo(x, page.mediaBox.height - y)
        contentStream.lineTo(width, page.mediaBox.height - y)
        contentStream.stroke()
    }

    private fun drawRectangle(
        x: Float,
        y: Float,
        width: Float,
        height: Float,
        borderColor: PDColor,
        fillColor: PDColor,
        labelText: String = ""
    ) {
        // Set the fill color
        contentStream.setNonStrokingColor(fillColor)
        contentStream.addRect(x, page.mediaBox.height - y - height, width, height)
        contentStream.fill()

        // Set the border color
        contentStream.setStrokingColor(borderColor)
        contentStream.setLineWidth(0.8f)
        contentStream.addRect(x, page.mediaBox.height - y - height, width, height)
        contentStream.stroke()

        // Add the label text if it's not empty
        if (labelText.isNotEmpty()) {
            val textWidth = (regularFont.getStringWidth(labelText) / 1000 * fontSize8) + 6
            val textHeight = (regularFont.fontDescriptor.fontBoundingBox.height / 1000 * fontSize8) + 6

            // Calculate the position of the text
            val rectX = x + 5
            val rectY = y - 5
            val textX = x + 8
            val textY = y + 2

            // Draw a white rectangle for the text background
            contentStream.setNonStrokingColor(WHITE_FILL_COLOR)
            contentStream.addRect(rectX, page.mediaBox.height - rectY - textHeight, textWidth, textHeight)
            contentStream.fill()

            // Add the text
            addText(labelText, textX, textY, regularFont, fontSize8, BLACK_TEXT_COLOR)
        }
    }

    private fun saveDocument(): ByteArrayOutputStream {
        val output = ByteArrayOutputStream()
        pdDocument.save(output)
        pdDocument.close()
        return output
    }
}

fun main(args: Array<String>) {
    printElapsedTimeInSeconds({
        val payload = Payload(
            customerName = "Mr. Lorene Dach ooh0iiecz2n",
            accountNumber = "77770400004783",
            adviceDocumentNumber = "7777040000478306112023",
            createdDate = "6 November 2023",
            tenure = "1 Bulan",
            maturityDate = "5 Februari 2025",
            nisbahRate = "16,21%",
            principalAmount = "Rp2.000.000",
            principalAmountInWords = "Dua juta rupiah",
            rollOverMode = "Diperpanjang otomatis dengan saldo pokok dan bagi hasil",
            contract = "Mudharabah Muthlaqah",
        )
        val documentCreator = PdfDocumentCreator(payload)
        val bytes = documentCreator.createDocument()

        File("${Paths.get("").toAbsolutePath().toString()}/results/pdfbox/pdfbox.pdf")
            .writeBytes(bytes.toByteArray())
    }, "pdfbox generate texts and image")
}
